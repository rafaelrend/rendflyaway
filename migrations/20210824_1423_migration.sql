CREATE TABLE `tipo_cadastro_basico` (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
	`descricao` VARCHAR(300) NULL DEFAULT NULL COMMENT 'Descricao' COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;


CREATE TABLE `cadastro_basico` (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
	`descricao` TEXT NULL COMMENT 'Descricao' COLLATE 'utf8_unicode_ci',
	`id_tipo_cadastro_basico` INT(11) NULL DEFAULT NULL,
	`campo1` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`campo2` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`campo3` TEXT NULL COLLATE 'utf8_unicode_ci',
	`codigo` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`id_cliente` BIGINT(20) NULL DEFAULT NULL,
	`id_usuario_cadastro` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `tipo_cadastro_basico` (`id_tipo_cadastro_basico`),
	INDEX `ix_cliente` (`id_cliente`, `id_tipo_cadastro_basico`),
	INDEX `ix_usuario_cadastro` (`id_usuario_cadastro`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=78
;