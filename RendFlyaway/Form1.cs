﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace RendFlyaway
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();



            carregaConexoes();

            string pasta_scripts = System.Configuration.ConfigurationManager.AppSettings["pasta_migration"];
            txtPasta.Text = pasta_scripts;

            string formato = System.Configuration.ConfigurationManager.AppSettings["formato"];

            FlyExecute.onMessage += FlyExecute_onMessage;
            ConnAccess.onMessage += FlyExecute_onMessage;
        }

        private void FlyExecute_onMessage(object sender, EventArgs e)
        {
            setMensagem(sender.ToString());
        }

        DataTable dt_conexao;
        public void carregaConexoes()
        {
            string pasta = Application.StartupPath;

            pasta = pasta.Replace("\\bin\\Debug", "");
            pasta = pasta.Replace("\\bin\\Release", "");

            if (!System.IO.File.Exists(pasta + "\\conexoes.txt"))
            {
                MessageBox.Show(" É necessário um arquivo " + pasta + "\\conexoes.txt" + " contendo as conexões. ");
                return;
            }


            if (!System.IO.File.Exists(pasta + "\\create_table_migration.txt"))
            {
                MessageBox.Show(" É necessário um arquivo " + pasta + "\\create_table_migration.txt" + " contendo o script de criação do migration. ");
                return;
            }

            if (System.Configuration.ConfigurationManager.AppSettings["pasta_migration"] == null )
            {

                MessageBox.Show(" Informe a chave  pasta_migration no arquivo app.config ");
                return;
            }
            if (System.Configuration.ConfigurationManager.AppSettings["tabela_fly"] == "")
            {

                MessageBox.Show(" Informe a chave  tabela_fly no arquivo app.config ");
                return;
            }
            string texto = System.IO.File.ReadAllText(pasta + "\\conexoes.txt", System.Text.Encoding.GetEncoding("ISO-8859-1"));

            string[] linhas = texto.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);


            dt_conexao = new DataTable();

            dt_conexao.Columns.Add("Nome", typeof(string));
            dt_conexao.Columns.Add("Servidor", typeof(string));
            dt_conexao.Columns.Add("Ativo", typeof(string));
            dt_conexao.Columns.Add("Banco", typeof(string));
            dt_conexao.Columns.Add("Usuario", typeof(string));
            dt_conexao.Columns.Add("Senha", typeof(string));
            dt_conexao.Columns.Add("Porta", typeof(string));


            for (int i = 0; i < linhas.Length; i++)
            {
                if (linhas[i].Trim() == String.Empty)
                    continue;

                if (linhas[i].Trim().Substring(0, 1) == "#")
                    continue;

                string[] colunas = linhas[i].Split(new string[] { "\t" }, StringSplitOptions.None);

                DataRow dr = dt_conexao.NewRow();

                for (int yy = 0; yy < colunas.Length; yy++)
                {
                    dr[yy] = colunas[yy];

                }

                dt_conexao.Rows.Add(dr);
            }

            this.dataGridView1.DataSource = dt_conexao;


            cmbConexaoSelecionada.DisplayMember = "Nome";
            cmbConexaoSelecionada.ValueMember = "Nome";
            cmbConexaoSelecionada.DataSource = dt_conexao;


        }


        delegate void MensagemCallback(string text);
        void setMensagem(string text)
        {
            if (txLog.InvokeRequired)
            {
                MensagemCallback d = new MensagemCallback(setMensagem);
                this.Invoke(d, new object[] { text });

            }
            else
            {
                if ( text.IndexOf(System.Environment.NewLine) < 0)
                {
                    text = System.Environment.NewLine + text;
                }
                txLog.Text += text + " - " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
            }

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string pasta = Application.StartupPath;

            pasta = pasta.Replace("\\bin\\Debug", "");
            pasta = pasta.Replace("\\bin\\Release", "");

            string formato = System.Configuration.ConfigurationManager.AppSettings["formato"];
            string nome_tabela_fly = System.Configuration.ConfigurationManager.AppSettings["tabela_fly"];


            if (dt_conexao.Rows.Count > 0)
            {
                string conexao = formato;
                for (int yy = 0; yy < dt_conexao.Columns.Count; yy++)
                {
                    int i = index_selecionado;
                    conexao = conexao.Replace("{" + yy.ToString() + "}", dt_conexao.Rows[i][dt_conexao.Columns[yy]].ToString());
                }
                setMensagem("Conectando em " + conexao);
                try
                {
                    System.Data.Odbc.OdbcConnection conn = ConnAccess.getConn(conexao);
                    DataRow drfly = null;
                    try
                    {
                        drfly = ConnAccess.GetNewRow(nome_tabela_fly, conn);
                    }
                    catch(Exception exp)
                    {
                        setMensagem(System.Environment.NewLine + " Não temos a tabela " + nome_tabela_fly + " no banco de dados. Vamos criar ");
                        //
                        // string pasta_migration = System.Configuration.ConfigurationManager.AppSettings["pasta_migration"];

                        string create_table_migration = System.IO.File.ReadAllText(pasta + "\\create_table_migration.txt",  System.Text.Encoding.GetEncoding("utf-8"));
                        ConnAccess.ExecuteCommand(create_table_migration, conn);

                        setMensagem(System.Environment.NewLine + " Tabela " + nome_tabela_fly + " criada ");

                        drfly = ConnAccess.GetNewRow(nome_tabela_fly, conn);

                    }

                    FlyExecute.tabela_flyaway = nome_tabela_fly;
                    FlyExecute.passAway = !onErrorPare;
                    if ( drfly != null)
                    {
                        FlyExecute.executeFlyAway(txtPasta.Text, drfly, conn);
                    }
                }
                catch (Exception exp)
                {
                    setMensagem(System.Environment.NewLine + "::: Problema na conexão - " + conexao + " / " + exp.Message);
                }
            }

        }



        int index_selecionado = 0;
        bool onErrorPare = false;
        private void button1_Click(object sender, EventArgs e)
        {
            if (cmbConexaoSelecionada.SelectedIndex < 0)
            {
                MessageBox.Show("Selecione a conexão!");
                return;
            }

            onErrorPare = comboBox1.SelectedIndex > 0;
            index_selecionado = cmbConexaoSelecionada.SelectedIndex;
            backgroundWorker1.RunWorkerAsync();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            string pasta_migration = System.Configuration.ConfigurationManager.AppSettings["pasta_migration"];

            String usuarioNome = Environment.UserName.ToLower().Replace(" ", "").Replace("-", "_");

            System.Text.Encoding encSaida = new System.Text.UTF8Encoding(false);
            String novadata = DateTime.Now.ToString("yyyyMMdd_HHmm") + "_" + usuarioNome + "_migration.sql";

            //System.IO.File.WriteAllText(pasta_migration +"\\"+ novadata, "", encSaida);
            System.IO.File.Copy(Application.StartupPath + "\\modelo.sql", pasta_migration + "\\" + novadata, true);
            MessageBox.Show("Criado o arquivo " + pasta_migration + "\\" + novadata + " - coloque nele o script de banco para ser executado ");


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
