﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;

namespace RendFlyaway
{
    public class FlyExecute
    {
        public static string tabela_flyaway = "";
        public static bool passAway = false;

        public static event EventHandler onMessage;

        public static Encoding GetEncoding(string filename)
        {
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }

            // Analyze the BOM
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe && bom[2] == 0 && bom[3] == 0) return Encoding.UTF32; //UTF-32LE
            if (bom[0] == 0xff && bom[1] == 0xfe) return Encoding.Unicode; //UTF-16LE
            if (bom[0] == 0xfe && bom[1] == 0xff) return Encoding.BigEndianUnicode; //UTF-16BE
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return new UTF32Encoding(true, true);  //UTF-32BE

            // We actually have no idea what the encoding is if we reach this point, so
            // you may wish to return null instead of defaulting to ASCII
            // return Encoding.GetEncoding("ISO-8859-1"); // Encoding.ASCII;
             return new UTF8Encoding(false);
        }

        public static void executeFlyAway(string path, DataRow drFly, System.Data.Odbc.OdbcConnection conn)
        {
            List<String> arquivos = System.IO.Directory.GetFiles(path).Where(file => file.ToLower().EndsWith("sql") || file.ToLower().EndsWith("txt")).OrderBy(file => file).ToList();
            //System.Array.Sort(arquivos);

            int ordem = ConnAccess.getMax("ordem", tabela_flyaway, conn);
            ordem++;
            int qtde = 0;

            

            foreach(string arquivo in arquivos)
            {
                string file_name = System.IO.Path.GetFileName(arquivo);

                conn.Close();
                conn.Open();

                System.Text.Encoding encEntrada = GetEncoding(arquivo); //  new UTF8Encoding(false);

                string sql = "select id from " + tabela_flyaway + " where file='" + file_name + "' and status = 1 ";
                object idtmp = ConnAccess.getScalar(sql, conn);
                if ( idtmp == null || idtmp == DBNull.Value)
                {
                    onMessage?.Invoke(System.Environment.NewLine + "Executando o arquivo " + file_name, null);
                    String executeSql = System.IO.File.ReadAllText(arquivo, encEntrada);
                    bool sucesso = false;
                    try
                    {
                        ConnAccess.ExecuteCommands(executeSql, conn, passAway);
                        sucesso = true;
                    }
                    catch(Exception exp)
                    {
                        onMessage?.Invoke(System.Environment.NewLine +  "[ERRO] Houve erro executando o arquivo " + file_name + "::" + exp.Message, null);
                        sucesso = false;
                    }

                    if ( sucesso || passAway)
                    {
                        sql = "select id from " + tabela_flyaway + " where file='" + file_name + "' ";
                        idtmp = ConnAccess.getScalar(sql,conn);
                        qtde++;

                        if (sucesso)
                        {

                            onMessage?.Invoke(System.Environment.NewLine + "Sucesso! Salvando no banco.. ", null);
                        } else
                        {
                            onMessage?.Invoke(System.Environment.NewLine + "Apesar do erro vamos salvar no banco.. ", null);
                        }

                        ConnAccess.cleanDataRow(ref drFly);

                        drFly["file"] = file_name;
                        drFly["status"] = 1;
                        drFly["data"] = DateTime.Now;
                        drFly["ordem"] = ordem;


                        if ( idtmp != null && idtmp != DBNull.Value)
                        {
                            drFly["id"] = idtmp;
                            ConnAccess.Update(drFly, conn);
                        }else
                        {
                            ConnAccess.Insert(drFly, true, conn);
                        }

                    }

                    onMessage?.Invoke(System.Environment.NewLine + "Feito para o arquivo " + file_name, null);
                }

            }

            onMessage?.Invoke(System.Environment.NewLine + "Feito para " + qtde + " arquivos ", null);

        }
    }
}
