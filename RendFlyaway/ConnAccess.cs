﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Windows.Forms;

namespace RendFlyaway
{
    public class ConnAccess
    {
        //private global::System.Data.OleDb.OleDbDataAdapter Adapter
        //protected global::System.Data.OleDb.OleDbCommand[] CommandCollection

        public static OdbcConnection myconn;

        public static event EventHandler onMessage;

        public static OdbcConnection getConn(string querystring)
        {
            OdbcConnection o_myconn = new OdbcConnection(querystring);
            o_myconn.Open();

            return o_myconn;
        }

        public static OdbcConnection getConn(bool forcaNovo)
        {


            string loc = Application.StartupPath.Replace("\\bin\\Debug", "");

            string querystring = "Driver={Microsoft Access Driver (*.mdb)};Dbq=" + loc + "\\db.mdb;";


            if (forcaNovo && myconn != null)
            {
                if (myconn.State == ConnectionState.Open)
                    myconn.Close();


                myconn = null;
            }
            if (!forcaNovo && myconn != null)
                return myconn;

            if (myconn == null || forcaNovo)
            {

                myconn = new OdbcConnection(querystring);
                myconn.Open();
            }

            return myconn;
        }

        public static DataSet fetchAll(string sql, OdbcConnection conn)
        {

            OdbcDataAdapter da = new OdbcDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);

            return ds;

        }

        public static DataTable fetchDados(string sql, OdbcConnection conn)
        {

            OdbcDataAdapter da = new OdbcDataAdapter(sql, conn);
            DataTable ds = new DataTable();
            da.Fill(ds);

            return ds;

        }

        public static DataRow GetNewRow(string tabela, OdbcConnection conn)
        {
            string sql = "select * from " + tabela + " where 1 = 0 ";
            OdbcDataAdapter da = new OdbcDataAdapter(sql, conn);
            DataTable ds = new DataTable();
            ds.TableName = tabela;
            da.Fill(ds);          

            return ds.NewRow();

        }

        public static string getNumero(string exp)
        {
            string saida = "";
            System.Text.RegularExpressions.MatchCollection m =
                  System.Text.RegularExpressions.Regex.Matches(exp, "[0-9]");

            for (int i = 0; i < m.Count; i++)
            {
                saida += m[i].Value;
            }
            return saida;

        }

        public static bool isPrimaryKey(DataColumn dt, DataTable table)
        {

            DataColumn[] pks = table.PrimaryKey;

            for (int i = 0; i < pks.Length; i++)
            {
                if (pks[i].Equals(dt))
                {
                    return true;
                }
            }
            return false;
        }
        

        public static int getMax(string coluna, string tabela, OdbcConnection conn)
        {
            string sql = " select max(" + coluna + ") from " + tabela;



            OdbcCommand cmd = new OdbcCommand(sql, conn);

            object cod = cmd.ExecuteScalar();

            if (cod != null && cod.ToString() != String.Empty)
                return Convert.ToInt32(cod.ToString());

            return 0;


        }
        public static object ExecuteCommand(string sql, OdbcConnection conn)
        {

            OdbcCommand cmd = new OdbcCommand(sql, conn);

            object cod = cmd.ExecuteNonQuery();

            return cod;


        }

        public static object ExecuteCommands(string sql, OdbcConnection conn, bool onErrorNext = false )
        {

            string[] comands = sql.Split(';');
            object cod = null;

            string sqlteste = @"CREATE TABLE `tipo_cadastro_basico` (
	                                `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
	                                `descricao` VARCHAR(300) NULL DEFAULT NULL COMMENT 'Descricao' COLLATE 'utf8_unicode_ci',
	                                PRIMARY KEY (`id`)
                                )
                                COLLATE='utf8_unicode_ci'
                                ENGINE=InnoDB
                                AUTO_INCREMENT=1
                                ;";

           // OdbcCommand cmd2 = new OdbcCommand(sqlteste, conn);
           // cod = cmd2.ExecuteNonQuery();

            foreach (string comand in comands)
            {
                if ( comand.Trim() != "")
                {
                    try
                    {

                        OdbcCommand cmd = new OdbcCommand(comand + ";", conn);
                        cod = cmd.ExecuteNonQuery();
                        
                    }
                    catch(Exception exp)
                    {
                        onMessage?.Invoke("Erro ao tentar executar comando " + comand + " , erro: " + exp.Message, null);
                        if (onErrorNext)
                        {

                        }
                        else
                        {
                            throw exp;
                        }
                    }
                }
            }


            return cod;


        }
        public static object getScalar(string sql, OdbcConnection conn)
        {

            OdbcCommand cmd = new OdbcCommand(sql, conn);

            object cod = cmd.ExecuteScalar();

            return cod;


        }

        public static void cleanDataRow(ref DataRow dr)
        {
            for (int i = 0; i < dr.Table.Columns.Count; i++)
            {
                dr[dr.Table.Columns[i]] = DBNull.Value;
            }
        }

        /// <summary>
        /// Faz a atualização de um registro.
        /// </summary>
        /// <param name="dr"></param>
        public static void Update(DataRow dr, OdbcConnection conn)
        {
            string sql = " update " + dr.Table.TableName + " set ";
            string where = "";

            OdbcCommand cmd = new OdbcCommand();


            int conta = 0;
            for (int i = 0; i < dr.Table.Columns.Count; i++)
            {
                DataColumn coluna = dr.Table.Columns[i];

                if (ConnAccess.isPrimaryKey(dr.Table.Columns[i], dr.Table))
                {

                    continue;
                }

                if (conta > 0)
                    sql += ", ";

                conta++;

                sql += coluna.ColumnName + " = ? ";


                OdbcParameter par = new OdbcParameter(coluna.ColumnName,
                     ConnAccess.getTipo(coluna.DataType));

                if (dr[coluna] == null)
                    par.Value = DBNull.Value;
                else
                    par.Value = dr[coluna];

                cmd.Parameters.Add(par);

            }

            DataColumn[] pks = dr.Table.PrimaryKey;

            if ( pks.Length <= 0)
            {
                pks = new DataColumn[] { dr.Table.Columns["id"] };
            }

            for (int i = 0; i < pks.Length; i++)
            {
                DataColumn coluna = pks[i];

                if (where == "")
                    where += " where " + coluna.ColumnName + " = ? ";
                else
                    where += " and " + coluna.ColumnName + " = ? ";

                OdbcParameter par = new OdbcParameter(coluna.ColumnName,
                       ConnAccess.getTipo(coluna.DataType));

                if (dr[coluna] == null)
                    par.Value = DBNull.Value;
                else
                    par.Value = dr[coluna];

                cmd.Parameters.Add(par);

            }
            sql += where;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            cmd.Connection = conn; // ConnAccess.getConn(false);

            cmd.ExecuteNonQuery();

        }
        public static OdbcType getTipo(Type tipo)
        {

            if (tipo == typeof(Int16) || tipo == typeof(Int32) || tipo == typeof(Int64))
                return OdbcType.Int;

            if (tipo == typeof(DateTime))
                return OdbcType.DateTime;

            if (tipo == typeof(TimeSpan))
                return OdbcType.Timestamp;

            if (tipo == typeof(Boolean))
                return OdbcType.Bit;

            if (tipo == typeof(Char))
                return OdbcType.Char;

            if (tipo == typeof(String))
                return OdbcType.VarChar;

            if (tipo == typeof(Byte))
                return OdbcType.TinyInt;


            return OdbcType.VarChar;

        }

        /// <summary>
        /// Faz o cadastro de um registro.
        /// </summary>
        /// <param name="dr"></param>
        public static void Insert(DataRow dr, bool autoincrement, OdbcConnection conn)
        {
            string sql = " insert into " + dr.Table.TableName + " ( ";
            string where = "";

            OdbcCommand cmd = new OdbcCommand();


            int conta = 0;
            for (int i = 0; i < dr.Table.Columns.Count; i++)
            {
                DataColumn coluna = dr.Table.Columns[i];

                if (ConnAccess.isPrimaryKey(dr.Table.Columns[i], dr.Table) && autoincrement)
                {

                    continue;
                }


                if (conta > 0)
                {
                    sql += ", ";
                    where += ", ";
                }
                conta++;

                sql += coluna.ColumnName + " ";
                where += " ? ";


                OdbcParameter par = new OdbcParameter(coluna.ColumnName,
                     ConnAccess.getTipo(coluna.DataType));

                if (dr[coluna] == null)
                    par.Value = DBNull.Value;
                else
                    par.Value = dr[coluna];

                cmd.Parameters.Add(par);

            }

            sql += ") VALUES ( " + where + ") ";
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            cmd.Connection = conn;  // ConnAccess.getConn(false);

            cmd.ExecuteNonQuery();

        }


        /// <summary>
        /// Deleta um registro a partir de um data row e faz a persistência no banco de dados.
        /// </summary>
        /// <param name="dr"></param>
        public static void Delete(DataRow dr)
        {
            string sql = " delete from " + dr.Table.TableName + "  ";
            string where = "";

            OdbcCommand cmd = new OdbcCommand();


            DataColumn[] pks = dr.Table.PrimaryKey;

            for (int i = 0; i < pks.Length; i++)
            {
                DataColumn coluna = pks[i];

                if (where == "")
                    where += " where " + coluna.ColumnName + " = ? ";
                else
                    where += " and " + coluna.ColumnName + " = ? ";

                OdbcParameter par = new OdbcParameter(coluna.ColumnName,
                       ConnAccess.getTipo(coluna.DataType));

                if (dr[coluna] == null)
                    par.Value = DBNull.Value;
                else
                    par.Value = dr[coluna];

                cmd.Parameters.Add(par);

            }
            sql += where;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            cmd.Connection = ConnAccess.getConn(false);

            cmd.ExecuteNonQuery();

        }



        public static string getNomeColuna(DataSet ds)
        {
            return ds.Tables[0].Columns[0].ColumnName;
        }

    }
}
